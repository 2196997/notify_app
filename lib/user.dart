
import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:dbutils/sqllitedb.dart';

class User {

  int id;
  String name;
  String pass;
  String email;
  String type;

  User({this.id, this.name, this.pass, this.email, this.type});

  factory User.fromJson(Map<String, dynamic> json) => new User(
      id: json['id'],
      name: json['name'],
      pass: json['pass'],
      email: json['email'],
      type: json['type'],
  );

/*  static User _this;
  User._getInstance(): super();

  factory User() {
    if (_this == null) {
      _this = User._getInstance();
    }
    return _this;
  }

  @override
  Future<void> onCreate(Database db, int version) async {
    await db.execute("""
      CREATE TABLE User(
        id INTEGER PRIMARY KEY,
        name TEXT,
        pass TEXT,
        type TEXT      
      )
    """);
  }

  @override
  String get dbName => 'testing.db';

  @override
  int get version => 1;

  void save(String table) {
    save(table);
  }

  Future<List<Map>> getUsers() async {
    return await this.rawQuery('SELECT * FROM User');
  }*/

  String getUser() { return name; }
  String getEmail() { return email; }
  String getPass() { return pass; }
  int getId() { return id; }

}

class Notifications {

  final int number;
  final String title;
  final String content;

  Notifications(this.number, this.title, this.content);

}

class Events {

  final int id;
  final String name;
  final String content;
//  final DateTime from;
//  final DateTime to;

  Events(this.id, this.name, this.content /*, this.from, this.to*/);

}