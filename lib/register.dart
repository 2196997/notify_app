import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:notify_app/user.dart';
import 'package:notify_app/utilities.dart';

List<User> listOfUsers;

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  ValueNotifier<double> _process = ValueNotifier<double>(1.0);
  bool _userType = false, _firstLayout = false, _secondLayout = false;
  var userList = ['STUDENT', 'TEACHER'];

  String id, idErr;
  String name, nameErr;
  String email, emailErr;
  String pass, passErr;
  String cpass, cpassErr;
  String userType;

  final _id = new TextEditingController();
  final _name = new TextEditingController();
  final _email = new TextEditingController();
  final _pass = new TextEditingController();
  final _cpass = new TextEditingController();

  Widget template(String text, String icon) {
    return Card(
      color: Color.fromRGBO(66, 92, 89, 90),
      margin: EdgeInsets.fromLTRB(15, 0, 0, 0),
      child: userButton(text, icon),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
            child: logoContainer(),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.fromLTRB(50, 0, 0, 0),
            child: Text(
              'Sign up',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Mont',
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Center(
            child: Container(
              width: 320,
              child: RoundedProgressBar(
                  height: 5.0,
                  milliseconds: 1000,
                  percent: _process.value,
                  margin: EdgeInsets.symmetric(vertical: 16),
                  borderRadius: BorderRadius.circular(40),
                  theme: RoundedProgressBarTheme.yellow
              ),
            ),
          ),
          AnimatedCrossFade(
            firstChild: chooseType(),
            secondChild: firstLayout(),
            crossFadeState: !_userType ? CrossFadeState.showFirst : CrossFadeState.showSecond,
            duration: const Duration(milliseconds: 0),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
            child: clearOrGoRow(),
          ),
          Container(
            alignment: Alignment.bottomRight,
            child: RawMaterialButton(
              onPressed: () => _dialogChange(),
              child: Icon(Icons.help_outline, size: 30, color: Colors.white30),
              shape: CircleBorder(),
              elevation: 4.0,
            ),
          )
        ],
      ),
      backgroundColor: Color.fromRGBO(66, 92, 89, 100),
    );
  }

  /// returns the container of the logo
  logoContainer() {
    return Center(
      child: Container(
        width: 120,
        height: 120,
        margin: EdgeInsets.all(24),
        padding: EdgeInsets.only(top: 24),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: new AssetImage('assets/icons/icon.png'),
            fit: BoxFit.cover,
          ),
          shape: BoxShape.rectangle,
        ),
      ),
    );
  }

  _dialogChange() {
    if (!_userType) 
      alertDialog('To Continue', 'Choose your corresponding type to continue with the registration.');
    else
      if (!_firstLayout)
        alertDialog('To Continue', 'Enter your identification number (from school) and your name (First Name, Middle Initial, Last Name).');
      else
        if (!_secondLayout)
          alertDialog('To Continue', 'Enter your password and make sure they are equal.');
  }

  _unclick() {
    setState(() {
      if (_userType)
        _userType = false;

      if (_firstLayout) {
//        _firstLayout = false;
        _id.clear();
        _name.clear();
      }

      if (_secondLayout) {
//        _secondLayout = false;
        _pass.clear();
        _cpass.clear();
        _process.value-=33;
      }
      _process.value-=33;
    });
  }

  _userTypePushed(String text) {
    if (text == userList[0])
      userType = userList[0];
    else
      userType = userList[1];

    setState(() {
      _userType = true;
      _process.value+=33;
    });
  }

  _registerClicked() {
    if (_userType == false) {
      alertDialog('To Continue', 'No user type chosen. To continue choose your affiliation.');
    } else {
      if (_firstLayout == false) {
        if (_id.toString().isNotEmpty && _name.toString().isNotEmpty) {
          setState(() {
            _firstLayout = true;
            _process.value+=33;
          });
        } else {
          alertDialog('To Register', 'If the progress bar above has not been filled, then you will have to fill out it out as you choose your choice.');
        }
      } else {
        if (pass != cpass) {
          alertDialog('Passwords Do Not Match', 'The entered passwords do not match, try again.');
        }
      }
    }

  }

  firstLayout() {
    return AnimatedCrossFade(
      firstChild: enterFirstCard(),
      secondChild: enterSecondCard(),
      crossFadeState: _firstLayout ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      duration: const Duration(milliseconds: 10),
    );
  }

  enterFirstCard() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Center(child: inputContainer('password', true, Icons.lock_outline, _pass, pass, passErr)),
        Center(
          child: Container(
            width: 300,
            child: TextFormField(
              controller: _name,
              style: TextStyle(
                  color: Colors.white70,
                  fontFamily: 'MHeavy'
              ),
              decoration: InputDecoration(
                  labelText: 'confirm password',
                  labelStyle: TextStyle(
                      color: Colors.white70),
                  prefixIcon: Icon(Icons.check),
                  errorText: cpassErr,
              ),
              obscureText: true,
              validator: (value) {
                if (value.isEmpty)
                  return cpassErr = 'Empty';
                if (value != pass)
                  return cpassErr = 'Passwords do not match';
                else
                  return null;
              },
              onSaved: (value) => cpass = value.trim(),
            ),
          ),
        )
      ],
    );
  }

  enterSecondCard() {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Center(child: idContainer('identification number', Icons.confirmation_number, _id, idErr)),
        Center(child: inputContainer('name', false, Icons.person_outline, _name, name, nameErr)),
        Center(
          child: Container(
            width: 300,
            child: TextFormField(
              autofocus: false,
              keyboardType: TextInputType.emailAddress,
              controller: _email,
              style: TextStyle(color: Colors.white70, fontFamily: 'MHeavy'),
              decoration: InputDecoration(
                labelText: 'email',
                labelStyle: TextStyle(color: Colors.white70),
                prefixIcon: Icon(Icons.email),
                errorText: emailErr,
              ),
            ),
          )
        ),
      ],
    );
  }

  idContainer(String hint, IconData icon, TextEditingController variable, String error) {
    return Container(
      width: 300,
      child: TextFormField(
        controller: variable,
        style: TextStyle(
            color: Colors.white70,
            fontFamily: 'MHeavy'
        ),
        decoration: InputDecoration(
          labelText: hint,
          labelStyle: TextStyle(
            color: Colors.white70),
          prefixIcon: Icon(icon),
          errorText: error
        ),
        keyboardType: TextInputType.numberWithOptions(),
        validator: (value) => value.isEmpty ? 'ID cannot be empty' : null,
        onSaved: (value) => id = value.trim(),
      ),
    );
  }

  inputContainer(String hint, bool value, IconData icon, TextEditingController variable, String varName, String error) {
    return Container(
      width: 300,
      child: TextFormField(
        controller: variable,
        style: TextStyle(
            color: Colors.white70,
            fontFamily: 'MHeavy'
        ),
        decoration: InputDecoration(
          labelText: hint,
          labelStyle: TextStyle(
            color: Colors.white70),
          prefixIcon: Icon(icon),
          errorText: error
        ),
        obscureText: value,
        validator: (value) {
          if (value.isEmpty)
            return error = 'Empty';
          else
            return null;
        },
        onSaved: (value) => varName = value.trim(),
      ),
    );
  }

  chooseType() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      height: 226,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          template('STUDENT', 'assets/icons/student-icon.png'),
          template('TEACHER', 'assets/icons/teacher-icon.png')
        ],
      ),
    );
  }

  userButton(String text, String icon) {
    return RaisedButton(
      color: Color.fromRGBO(66, 92, 89, 90),
      child: Column(
        children: <Widget>[
          Container(
            width: 150,
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage(icon), fit: BoxFit.cover),
            ),
          ),
          Text(
            text,
            style: TextStyle(
              color: Colors.white70,
              fontFamily: 'MHEAVY',
              fontSize: 20
            ),
          ),
        ],
      ),
      onPressed: () => _userTypePushed(text),
    );
  }

  clearOrGoRow() {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Icon(Icons.arrow_back, color: Colors.white),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        FlatButton(
          onPressed: () => _unclick(),
          child: Icon(Icons.clear, color: Colors.white),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        RaisedButton(
          onPressed: () => _registerClicked(),
          child: Row(
            children: <Widget>[
              Text(
                '  REGISTER ',
                style: TextStyle(
                  color: Colors.black87,
                  fontFamily: 'MHeavy',
                ),
              ),
              Icon(Icons.arrow_forward)
            ],
          ),
          color: Colors.white70,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
      ],
    );
  }

  alertDialog(String text, String content) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(text),
          content: Text(content),
          actions: <Widget>[
            FlatButton(onPressed: () => Navigator.of(context).pop(), child: Text('CLOSE'))
          ],
        );
      }
    );
  }

}
