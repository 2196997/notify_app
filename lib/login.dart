import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notify_app/register.dart';
import 'package:notify_app/student_home.dart';
import 'package:notify_app/teacher_home.dart';
import 'package:notify_app/utilities.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  TextEditingController _username = new TextEditingController();
  TextEditingController _password = new TextEditingController();

  String userName;
  String passWord;

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        shrinkWrap: true,
        children: <Widget>[
          Center(
            child: Padding(
              padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
              child: logoContainer(),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.fromLTRB(50, 0, 0, 0),
            child: Text(
              'Sign in',
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Mont',
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Center(child: inputContainer('user id or email', false, Icons.person, _username, userName)),
          Center(child: inputContainer('password', true, Icons.lock_outline, _password, passWord)),
          Center(child: flatTextContainer('forgot password?', null)),
          Center(child: clearOrGoRow()),
          Center(child: flatTextContainer('no account? sign up', MaterialPageRoute(builder: (context) => SignUp()))),
          Container(
            alignment: Alignment.bottomRight,
            child: RawMaterialButton(
              onPressed: () => alertDialog('To Continue', 'You must be a registered user to continue with the application.'),
              child: Icon(Icons.help_outline, size: 30, color: Colors.white30),
              shape: CircleBorder(),
              elevation: 4.0,
            ),
          )
        ],
      ),
      backgroundColor: Color.fromRGBO(66, 92, 89, 100),
    );
  }

  inputContainer(String hint, bool val, IconData icon, TextEditingController variable, String varia) {
    return Container(
      width: 300,
      child: TextFormField(
        maxLines: 1,
        autofocus: false,
        controller: variable,
        onSaved: (value) => varia = value.trim(),
        style: TextStyle(
          color: Colors.white70,
          fontFamily: 'MHeavy'
        ),
        decoration: InputDecoration(
          labelText: hint,
          labelStyle: TextStyle(
            color: Colors.white70,
          ),
          prefixIcon: Icon(icon),
        ),
        obscureText: val,
        validator: (value) => value.isEmpty ? '$hint cannot be empty.' : null,
      ),
    );
  }

  _buttonChange(MaterialPageRoute route) {
    if (route == null)
      return alertDialog('Forgot Password?', 'Contact your administrator');
    else
      Navigator.push(context, route);

  }

  flatTextContainer(String text, MaterialPageRoute route) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
      child: FlatButton(
          onPressed: () { _buttonChange(route); },
          child: Text(
            text,
            style: TextStyle(
              color: Colors.white30,
              fontFamily: 'Mont',
              fontStyle: FontStyle.italic
            ),
          ),
      ),
    );
  }

  clearOrGoRow() {
    return ButtonBar(
      alignment: MainAxisAlignment.center,
      children: <Widget>[
        FlatButton(
          onPressed: () {
            _username.clear();
            _password.clear();
          },
          child: Icon(Icons.clear, color: Colors.white),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
        RaisedButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => TeacherHome())); },
          child: Row(
            children: <Widget>[
              Text(
                '  LOGIN ',
                style: TextStyle(
                  color: Colors.black87,
                  fontFamily: 'MHeavy',
                ),
              ),
              Icon(Icons.arrow_forward)
            ],
          ),
          color: Colors.white70,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
        ),
      ],
    );
  }

  alertDialog(String text, String desc) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(text),
            content: Text(desc),
            actions: <Widget>[
              FlatButton(onPressed: () => Navigator.of(context).pop(), child: Text('CLOSE'))
            ],
          );
        }
    );
  }

}
