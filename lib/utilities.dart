import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:notify_app/user.dart';
import 'package:path_provider/path_provider.dart';

List<Notifications> notifs = new List();
List<Events> events = new List();



getNotifs() {
  return notifs;
}

getEvents() {
  return events;
}

Future<String> _read() async {
  String text;
  try {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/notificationList.txt');
    text = await file.readAsString();
  } catch (e) {
    print("Couldn't read file");
  }
  return text;
}

_write(String text) async {
  final directory = await getApplicationDocumentsDirectory();
  final file = File('${directory.path}/my_file.txt');
  await file.writeAsString(text);
}

logoContainer() {
  return Center(
      child: Container(
        width: 120,
        height: 120,
        margin: EdgeInsets.all(24),
        padding: EdgeInsets.only(top: 24),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: new AssetImage('assets/icons/icon.png'),
            fit: BoxFit.cover,
          ),
          shape: BoxShape.rectangle,
        ),
      )
  );
}

sendTo() {
  return Container(
    child: Text('TO:',
      style: TextStyle(color: Colors.white, fontFamily: 'MHeavy', fontSize: 20),
    ),
  );
}

additionalNote(String text) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
      Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: Text('ADDITIONAL $text:' ,
            style: TextStyle(color: Colors.white, fontFamily: 'MHeavy', fontSize: 20)),
      ),
      Container(
        margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: TextField(
          decoration: InputDecoration(
            hintText: 'add description...',
            hintStyle: TextStyle(color: Colors.white70, fontFamily: 'Chaparral')
            ),
          ),
        ),
      ],
    ),
  );
}

bool validateStructure(String value){
  String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  RegExp regExp = new RegExp(pattern);
  return regExp.hasMatch(value);
}