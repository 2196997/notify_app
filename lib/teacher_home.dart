import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:notify_app/user.dart';
import 'package:notify_app/utilities.dart';

List<Notifications> alerts = getNotifs();
List<Notifications> filteredAlerts = new List();

List<Events> filteredEvents = new List();

class TeacherHome extends StatefulWidget {
  @override
  _TeacherHomeState createState() => _TeacherHomeState();
}

class _TeacherHomeState extends State<TeacherHome> {

  bool loading = true;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Home',
            style: TextStyle(color: Colors.white, fontFamily: 'Mont', fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search, color: Colors.white,),
              onPressed: () {},
            ),
          ],
          bottom: navigationBar(),
          backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        ),
        drawer: teacherDrawer(),
        body: bodyBar(),
      ),
    );
  }

  teacherDrawer() {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
              child: FlatButton.icon(onPressed: () {},
                  icon: Icon(Icons.person, color: Colors.black87),
                  label: Text('Profile',
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: 'MHeavy'),))
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.black87),
            title: Text('Home', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); Navigator.push(context, MaterialPageRoute(builder: (context) => TeacherHome())); },
          ),
          ListTile(
            leading: Icon(Icons.event_note, color: Colors.black87),
            title: Text('Events', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); Navigator.push(context, MaterialPageRoute(builder: (context) => eventPage())); },
          ),
          ListTile(
            leading: Icon(Icons.list, color: Colors.black87),
            title: Text('Lists', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); Navigator.push(context, MaterialPageRoute(builder: (context) => listPage())); },
          ),
          ListTile(
            leading: Icon(Icons.mail, color: Colors.black87),
            title: Text('Mail', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); Navigator.push(context, MaterialPageRoute(builder: (context) => mailPage())); },
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  alignment: Alignment.bottomLeft,
                  child: ListTile(
                    leading: Icon(Icons.settings, color: Colors.black87),
                    title: Text('Settings', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
                    onTap: () { Navigator.pop(context); },
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: ListTile(
                    leading: Icon(Icons.arrow_back_ios, color: Colors.black87),
                    title: Text('Sign Out', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
                    onTap: () { Navigator.pop(context); },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  navigationBar() {
    return TabBar(
      unselectedLabelColor: Colors.white,
      indicatorColor: Colors.amberAccent,
      labelColor: Colors.amberAccent,
      labelStyle: TextStyle(fontFamily: 'MHeavy', fontSize: 12.5),
      tabs: [
        Tab(icon: Icon(Icons.notifications), text: 'Alerts',),
        Tab(icon: Icon(Icons.event), text: 'Events',),
        Tab(icon: Icon(Icons.mail), text: 'Mail',),
      ],
    );
  }

  bodyBar() {
    return TabBarView(
      children: <Widget>[
        Center(child: alertContainer()),
        Center(child: eventContainer()),
        Center(child: mailContainer()),
      ],
    );
  }

  listNavigationBar() {
    return TabBar(
      unselectedLabelColor: Colors.white,
      indicatorColor: Colors.amberAccent,
      labelColor: Colors.amberAccent,
      labelStyle: TextStyle(fontFamily: 'MHeavy', fontSize: 12.5),
      tabs: [
        Tab(icon: Icon(Icons.person_outline), text: 'People',),
        Tab(icon: Icon(Icons.notifications_none), text: 'Notifications',),
      ],
    );
  }

  listPage() {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Lists',
            style: TextStyle(color: Colors.white, fontFamily: 'Mont', fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search, color: Colors.white,),
              onPressed: () {},
            ),
          ],
          bottom: listNavigationBar(),
          backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        ),
        drawer: teacherDrawer(),
        body: TabBarView(
          children: <Widget>[
            Center(child: alertContainer()),
            Center(child: eventContainer()),
          ],
        ),
      ),
    );
  }

  mailPage() {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Mail',
          style: TextStyle(color: Colors.white, fontFamily: 'Mont', fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white,),
            onPressed: () {},
          ),
        ],
        backgroundColor: Color.fromRGBO(66, 92, 89, 100),
      ),
      drawer: teacherDrawer(),
      body: mailContainer(),
    );
  }

  eventPage() {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Events',
          style: TextStyle(color: Colors.white, fontFamily: 'Mont', fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search, color: Colors.white,),
            onPressed: () {},
          ),
        ],
        backgroundColor: Color.fromRGBO(66, 92, 89, 100),
      ),
      drawer: teacherDrawer(),
      body: eventContainer(),
    );
  }

  _alertState(List value, List arrTo) {
    for (var i = 0; i < value.length; i++) {
      arrTo.add(new ListTile(title: Text(value[i].toString())));
    }

    setState(() {
      loading = false;
    });
  }

  alertContainer() {
    return Scaffold(
      body: ListView(
        children: loading?[]:alerts,
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'ADD',
        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddAlert())),
        backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        child: Icon(Icons.add, size: 30.5, color: Colors.white),
      ),
    );
  }

  var counter = 1;

  eventContainer() {
    return Scaffold(
      body: ListView.builder(
        itemCount:  18,
        itemBuilder: (BuildContext context, int index) {
          return new ListTile(
            title: Text('Event $counter'),
            onTap: () {print('sagiang'); setState(() {
              counter++;
            });},
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'ADD',
        onPressed: () {},
        backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        child: Icon(Icons.add, size: 30.5, color: Colors.white),
      ),
    );
  }

  mailContainer() {
    return Scaffold(
      body: ListView.builder(
        itemCount:  18,
        itemBuilder: (BuildContext context, int index) {
          return new ListTile(
            title: Text('MIKA'),
            onTap: () => print('sagiang'),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'ADD',
        onPressed: () {},
        backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        child: Icon(Icons.add, size: 30.5, color: Colors.white),
      ),
    );
  }

}

class AddAlert extends StatefulWidget {
  @override
  _AddAlertState createState() => _AddAlertState();
}

class _AddAlertState extends State<AddAlert> {

  TextEditingController _event = new TextEditingController();

  List<Events> eventList = new List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(66, 92, 89, 100),
      body: ListView(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
                alignment: Alignment.topLeft,
                child: Text(
                  'Invite ',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Mont',
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 130,
                width: 130,
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: logoContainer(),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
            alignment: Alignment.topLeft,
            child: Text('CHOOSE AN EVENT:',
              style: TextStyle(color: Colors.white, fontFamily: 'MHeavy', fontSize: 20),
            ),
          ),
          Center(child: eventListDisplay()),
          Container(
            margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
            alignment: Alignment.topLeft,
            child: Row(
              children: <Widget>[
                Text('CHOSEN EVENT:',
                  style: TextStyle(color: Colors.white, fontFamily: 'MHeavy', fontSize: 20)),
                TextFormField(
                  controller: _event,
                  decoration: InputDecoration(
                    hintText: 'CHOSEN EVENT'
                  ),
                )
              ],
            )
          ),
          Container(
            margin: EdgeInsets.fromLTRB(30, 0, 0, 0),
            child: sendTo() // send to particular lists
          ),
          Center(child: additionalNote('MESSAGES'),
          ),
        ],
      ),
    );
  }

  eventListDisplay() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      height: 200,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: eventList.length,
        itemBuilder: (context, index) {
          return Container(
            width: 200,
            child: Card(
              color: Color.fromRGBO(66, 92, 89, 98),
              child: Container(
                child: Center(child: Text(eventList[index].toString(), style: TextStyle(fontFamily: 'MHeavy'),)),
              ),
            ),
          );
        },
      ),
    );
  }

}

class AddMail extends StatefulWidget {
  @override
  _AddMailState createState() => _AddMailState();
}

class _AddMailState extends State<AddMail> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class AddEvent extends StatefulWidget {
  @override
  _AddEventState createState() => _AddEventState();
}

class _AddEventState extends State<AddEvent> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

