import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StudentHome extends StatefulWidget {
  @override
  _StudentHomeState createState() => _StudentHomeState();
}

class _StudentHomeState extends State<StudentHome> with SingleTickerProviderStateMixin {

  int count;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        drawer: studentDrawer(),
        appBar: AppBar(
          title: Text(
            'Notifs.',
            style: TextStyle(color: Colors.white, fontFamily: 'Mont', fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search, color: Colors.white,),
              onPressed: () {},
            ),
          ],
          bottom: navigationBar(),
          backgroundColor: Color.fromRGBO(66, 92, 89, 100),
        ),
        body: bodyBar(),
      ),
    );
  }

  studentDrawer() {
    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
              child: FlatButton.icon(onPressed: () {},
                  icon: Icon(Icons.person, color: Colors.black87),
                  label: Text('Profile',
                    style: TextStyle(
                        color: Colors.black87,
                        fontFamily: 'MHeavy'),))
          ),
          ListTile(
            leading: Icon(Icons.home, color: Colors.black87),
            title: Text('Home', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); },
          ),
          ListTile(
            leading: Icon(Icons.storage, color: Colors.black87),
            title: Text('Events', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); },
          ),
          ListTile(
            leading: Icon(Icons.mail, color: Colors.black87),
            title: Text('Mail', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
            onTap: () { Navigator.pop(context); },
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  alignment: Alignment.bottomLeft,
                  child: ListTile(
                    leading: Icon(Icons.settings, color: Colors.black87),
                    title: Text('Settings', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
                    onTap: () { Navigator.pop(context); },
                  ),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  child: ListTile(
                    leading: Icon(Icons.arrow_back_ios, color: Colors.black87),
                    title: Text('Sign Out', style: TextStyle(fontFamily: 'MHeavy', color: Colors.black87)),
                    onTap: () { Navigator.pop(context); },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  navigationBar() {
    return TabBar(
      unselectedLabelColor: Colors.white,
      indicatorColor: Colors.amberAccent,
      labelColor: Colors.amberAccent,
      labelStyle: TextStyle(fontFamily: 'MHeavy', fontSize: 12.5),
      tabs: [
        Tab(icon: Icon(Icons.call_received), text: 'Recent Events',),
        Tab(icon: Icon(Icons.all_inclusive), text: 'General Events',),
        Tab(icon: Icon(Icons.archive), text: 'Archived Events',),
      ],
    );
  }

  bodyBar() {
    return TabBarView(
      children: <Widget>[
        Center(child: Text('banana'),),
        Center(child: Text('yeyyy'),),
        Center(child: Text('lieee')),
      ],
    );
  }

}
