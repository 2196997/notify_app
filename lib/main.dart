import 'package:flutter/material.dart';
import 'package:notify_app/login.dart';

void main() => runApp(NotifyApp());

class NotifyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Banana Notify',
      darkTheme: ThemeData.dark(),
      home: Login()
    );
  }
}
